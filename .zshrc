#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ll='ls -lha'

function aurinstall() {
	if [ -d "~/builds/$1" ]; then
		echo "The directory ~/builds/$1 already exists"
		exit
	fi
	git clone https://aur.archlinux.org/$1.git ~/builds/$1
	( cd ~/builds/$1 && makepkg -is )
}

PS1='[\u@\h \W]\$ '
autoload -Uz compinit promptinit
compinit
promptinit

# This will set the default prompt to the walters theme
prompt walters
